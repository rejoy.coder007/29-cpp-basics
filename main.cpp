#include<iostream>
using namespace std;


class Mem
{
public:
    void show()
    {
        cout << "1\n";
    }
    void sleep(){
        cout<<"2";
    }
};


int main()
{
    //Interface obj;   //Compile Time Error
    Mem *b=new Mem();


    b->show();
    b->sleep();

    delete b;
}